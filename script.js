const liste = [];

$.get('liste.txt', function(data) {
	var lines = data.split("\n");

	$.each(lines, function(n, elem) {
		liste[n] = elem;
	});
});

const letter = "abcdefghijklmnopqrstuvwxyz";

function getRndInteger(min, max) {
	return Math.floor(Math.random() * (max - min + 1) ) + min;
}

function getQuestion() {
	$('.cat').text(liste[getRndInteger(0, liste.length)]);
	$('.lettre').addClass("hidden");
	$('.lettre').text(letter.charAt(getRndInteger(0, 25)));
	setTimeout(() => {
		$('.lettre').removeClass("hidden");
	}, 2000);
}

$(function() {
	 $(window).keypress(function(e) {
			if(e.which == 32) {
				getQuestion();
			}
	 });
});

